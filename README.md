# Desafio técnico

O proprietário de uma instituição de ensino deseja comercializar seus produtos através de um site.

Algumas regras de negócio devem ser observadas para a venda desses produtos;

Os produtos podem ser cursos, revisão de trabalhos acadêmicos, segunda chamada de provas.

Um produto deve conter:
* Nome;
* Descrição;
* Preço de ancoragem;
* Preço promocional;

Alguns tipos de produtos devem possuir uma configuração de pagamento.
Por exemplo:

    Trabalhos acadêmicos só podem ser parcelados em até 3x no cartão de crédito, não devem receber desconto no boleto ou pix;
    
    Cursos podem ser parcelados em até 12x cartão de crédito, devem receber 15% de desconto no pix e 5% no boleto;

Essas configurações de pagamento podem mudar com o tempo, então é interessante que sejam cadastradas e possam ser atualizadas.

Por tanto as configurações de pagamento devem possuir:
* Nome;
* Numero máximo de parcelas no cartão de crédito;
* Porcentagem de desconto para pagamento via pix;
* Porcentagem de desconto para pagamento via boleto;
* Valor mínimo de parcela;


Após criar os cadastros necessários, crie também uma página para exibir o produto, exibindo o nome, a descrição e o valor do produto para cada meio de pagamento.



Recomendações:

* Crie testes para o código sejam unitários ou de integração;
* Adicione ao seu projeto um arquivo readme explicando os passos para configurar e executar seu projeto;
* Crie commits pequenos em seu projeto.
